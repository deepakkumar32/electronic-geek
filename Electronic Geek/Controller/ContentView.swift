//
//  ContentView.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 16/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    let helpScreenArray: [Any] = [HelpScreen1.self, HelpScreen2.self]
    @State private var index: Int = 0
    
    var body: some View {
        ZStack() {
            SwiperView(helpScreenArray: self.helpScreenArray, index: self.$index)
            HStack() {
                Button(action: self.skipButtonAction) {
                    Text(" SKIP ")
                        .font(.custom(FontName.robotoMedium, size: 16))
                        .foregroundColor(.white)
                }
                .padding(.trailing, CGFloat(20))
            }
            .padding(.bottom, UIScreen.main.bounds.height-CGFloat(80))
            .padding(.leading, UIScreen.main.bounds.width-CGFloat(70))
            
            HStack(spacing: CGFloat(8)) {
                ForEach(0..<self.helpScreenArray.count) { index in
                    CircleButton(isSelected: Binding<Bool>(get: { self.index == index }, set: { _ in })) {
                        withAnimation {
                            self.index = index
                        }
                    }
                }
            }
            .padding(.top, UIScreen.main.bounds.height-CGFloat(60))
        }
    }
}

extension ContentView {
    
    fileprivate func goToLogin() {
        //        for family in UIFont.familyNames {
        //          let sName: String = family as String
        //          print("family: \(sName)")
        //
        //            for name in UIFont.fontNames(forFamilyName: sName) {
        //            print("name: \(name as String)")
        //          }
        //        }
         NotificationCenter.default.post(name: loginRootViewNotification, object: nil)
    }
    
    func skipButtonAction() {
        goToLogin()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
