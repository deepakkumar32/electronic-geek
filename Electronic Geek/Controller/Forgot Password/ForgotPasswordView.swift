//
//  ForgotPasswordView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 24/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct ForgotPasswordView: View {
    
    @State var email    = ""
    
    var body: some View {
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            VStack {
                
                ImageBoyFaceWithTitle(title: "Forgot Password", subTitle: "Reset Your Password")
                
                VStack(alignment: .center, spacing: 20) {
                    
                    TextField("Email", text: self.$email)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(30.0)
                    .shadow(radius: 10.0, x: 20, y: 10)
                    
                    Button(action: {
                        
                    }) {
                        Text("SUBMIT")
                            .font(.custom(FontName.robotoBold, size: 18))
                        .foregroundColor(.white)
                        .padding()
                            .frame(width: UIScreen.main.bounds.width/1/1.1)
                        .background(Color.yellow)
                        .cornerRadius(45.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                    }
                    .padding(.top, 15)
                    
                    Button(action: {
                        
                    }) {
                        Text("Go back to Login")
                            .font(.custom(FontName.robotoRegular, size: 14))
                            .foregroundColor(Color.white)
                            .padding()
                            .frame(width: UIScreen.main.bounds.width/3)
                    }
                    .padding(.top, 10)
                }
                .padding([.leading, .trailing], 27.5)
                .padding(.bottom, 20)
                .frame(width: UIScreen.main.bounds.width)
            }
        }
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
