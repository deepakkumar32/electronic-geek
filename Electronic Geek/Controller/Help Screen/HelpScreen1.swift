//
//  HelpScreen1.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 16/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct HelpScreen1: View {
    
    var body: some View {
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            VStack {
                HStack(alignment: .bottom) {
                    ImageHelpScreenBoy()
                    ImageHelpScreenChip()
                }
                .padding(.leading)
                VStack {
                    Text("WELCOME TO")
                        .font(.custom(FontName.robotoBlack, size: 20))
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                        .frame(width: UIScreen.main.bounds.width, height: 50, alignment: .center)
                        .padding(.bottom, -10)
                    Text("ELECTRONIC GEEK")
                        .font(.custom(FontName.plaster, size: 24))
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                    Divider()
                    Text("Challenge yourself")
                        .font(.custom(FontName.robotoRegular, size: 18))
                        .foregroundColor(Color.white)
                    Text("in the field of Electronics")
                        .font(.custom(FontName.robotoRegular, size: 18))
                        .foregroundColor(Color.white)
                }
                
            }
        }
    }
}

struct HelpScreen1_Previews: PreviewProvider {
    static var previews: some View {
        HelpScreen1()
    }
}

