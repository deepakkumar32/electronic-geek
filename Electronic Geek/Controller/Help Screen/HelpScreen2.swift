//
//  HelpScreen2.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 17/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct HelpScreen2: View {
    var body: some View {
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack(alignment: .bottom) {
                    ImageHelpScreenBoy()
                    ImageHelpScreenChip()
                }
                .padding(.leading)
                VStack {
                    Text("Level 1")
                         .font(.custom(FontName.robotoBlack, size: 20))
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                        .frame(width: UIScreen.main.bounds.width, height: 50, alignment: .center)
                        .padding(.bottom, -10)
                    Text("Logic Gate Mania")
                        .font(.custom(FontName.plaster, size: 24))
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                    Image("xor")
                        .aspectRatio(contentMode: .fill)
//                        .frame(width: UIScreen.main.bounds.width/7, height: UIScreen.main.bounds.width/8, alignment: .center)
                        .padding(.bottom, 20)
                    HStack(spacing: 0) {
                        Image("pig")
                        Image("pig")
                        Image("pig")
                    }.padding(.leading, 0)
                    Text("Challenge yourself")
                        .font(.custom(FontName.robotoRegular, size: 18))
                        .foregroundColor(Color.white)
                    Text("in the field of Electronics")
                        .font(.custom(FontName.robotoRegular, size: 18))
                        .foregroundColor(Color.white)
                }
                
            }
        }
    }
}

struct HelpScreen2_Previews: PreviewProvider {
    static var previews: some View {
        HelpScreen2()
    }
}
