//
//  SwiperView.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 20/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct SwiperView: View {
    let helpScreenArray: [Any]
    
    @Binding var index: Int
    @State private var offset: CGFloat = 0
    @State private var isUserSwiping: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .center, spacing: 0) {
                    ForEach(0..<self.helpScreenArray.count) { index in
                        PageView(helpScreenArray: self.helpScreenArray[index])
                            .frame(width: geometry.size.width,
                                   height: geometry.size.height)
                    }
                }
            }
            .content
            .offset(x: self.isUserSwiping ? self.offset : CGFloat(self.index) * -geometry.size.width)
            .frame(width: geometry.size.width, alignment: .leading)
            .gesture(
                DragGesture()
                    .onChanged({ value in
                        self.isUserSwiping = true
                        self.offset = value.translation.width + -geometry.size.width * CGFloat(self.index)
                    })
                    .onEnded({ value in
                        if value.predictedEndTranslation.width < geometry.size.width / 2, self.index < self.helpScreenArray.count - 1 {
                            self.index += 1
                        }
                        if value.predictedEndTranslation.width > geometry.size.width / 2, self.index > 0 {
                            self.index -= 1
                        }
                        withAnimation {
                            self.isUserSwiping = false
                        }
                    })
            )
        }
    }
}

struct PageView: View {
    let helpScreenArray: Any
    
    var body: some View {
        self.buildHelpScreenView(types: self.helpScreenArray)
    }
    
    func buildHelpScreenView(types: Any) -> AnyView {
        switch types.self {
        case is HelpScreen1.Type:
            return AnyView(HelpScreen1())
        case is HelpScreen2.Type:
            return AnyView(HelpScreen2())
        default:
            return AnyView(HelpScreen2())
        }
    }
}

struct CircleButton: View {
    @Binding var isSelected: Bool
    let action: () -> Void
    
    var body: some View {
        Button(action: {
            self.action()
        }) { Circle()
            .frame(width: 10, height: 10)
            .foregroundColor(self.isSelected ? Color.black : Color.white)
        }
    }
}
