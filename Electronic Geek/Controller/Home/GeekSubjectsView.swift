//
//  GeekSubjectsView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 04/08/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct GeekSubjectsView: View {
    @State var subViewCount: Int = 1
    
    var body: some View {
        ZStack {
            Color.yellow.edgesIgnoringSafeArea(.all)
            
            VStack {
                Text("LOGIC GATE MANIA")
                     .font(.custom(FontName.plaster, size: 25))
                     .foregroundColor(.white)
                     .padding(.top, 20)
                
                HStack {
                    Text("check")
//                        SUB
//                        \(self.subViewCount)
//                        """)
                        .frame(width: 80, height: 80, alignment: .center)
                        .background(Color.white)
                        .clipShape(Circle())
                        .font(.custom(FontName.robotoMedium, size: 18))
                        .foregroundColor(.yellow)
                        .multilineTextAlignment(.center)
                        .padding(.leading, 10)
                        .padding(.trailing, 20)
                    
                    Image("xor")
                        .resizable()
                        .clipped()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: UIScreen.main.bounds.size.width - 220, height: 120)
                }
                
                Text("Challenge yourself in the field of digital electronic")
                    .foregroundColor(.white)
                    .font(.custom(FontName.robotoRegular, size: 16))
                    .frame(width: 280)
                    .multilineTextAlignment(.center)
                    .lineLimit(nil)
                    .padding(.bottom, -5)
                
                HStack {
                    ForEach(0..<3) { index in
                        Image("pig")
                        .resizable()
                        .clipped()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 60, height: 40)
                    }
                }
            }
        }
    .cornerRadius(20)
    .clipped()
        .padding(.top, 20)
        .padding(.bottom, 20)
        .padding(.leading, 20)
        .padding(.trailing, 20)
    }
}

struct GeekSubjectsView_Previews: PreviewProvider {
    static var previews: some View {
        GeekSubjectsView()
    }
}


