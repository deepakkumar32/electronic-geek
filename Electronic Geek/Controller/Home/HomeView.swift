//
//  HomeView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 28/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @State var showMenu = false
    
    init() {
        UINavigationBar.appearance().backgroundColor = UIColor(red: 203/255.0, green: 147/255.0, blue: 47/255.0, alpha: 1.0)
       
        UINavigationBar.appearance().largeTitleTextAttributes = [
            .foregroundColor: UIColor.darkGray,
            .font : UIFont(name:"Papyrus", size: 5)!]
                 
        UINavigationBar.appearance().titleTextAttributes = [
            .font : UIFont(name: "HelveticaNeue-Thin", size: 20)!]
        
    }
    
    var body: some View {
        let drag = DragGesture()
        .onEnded {
            if $0.translation.width < -100 {
                withAnimation {
                    self.showMenu = false
                }
            }
        }
        return NavigationView {
            GeometryReader { geometry in
         //   ZStack {
                Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
                MainView(showMenu: self.$showMenu)
                .frame(width: geometry.size.width, height: geometry.size.height)
                .offset(x: self.showMenu ? geometry.size.width/2 : 0)
                .disabled(self.showMenu ? true : false)
                
                if self.showMenu {
                    MenuView()
                        .frame(width: geometry.size.width/2)
                        .transition(.move(edge: .leading))
                }
                }//}
            .gesture(drag)
            .navigationBarHidden(false)
            .navigationBarItems(leading:
            HStack {
                Button(action: {
                    withAnimation {
                        self.showMenu.toggle()
                    }
                }) {
                    Image(systemName: "line.horizontal.3")
                    .imageScale(.large)
                }//.foregroundColor(.pink)
            }
        )
        .navigationBarTitle(Text("Geek Subjects"), displayMode: .inline)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct MainView: View {
    
    @Binding var showMenu: Bool
    
    var body: some View {
      //  Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
        List {
            GeekSubjectsView(subViewCount: 1)
            GeekSubjectsView(subViewCount: 2)
        }
        .foregroundColor(.red)
        .background(Color.clear)
        .frame(width: UIScreen.main.bounds.width)
    }
}
