//
//  SessionStore.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 22/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI
import Firebase
import Combine

class SessionStore : ObservableObject {
    
    var didChange = PassthroughSubject<SessionStore, Never>()
    var session: User? { didSet { self.didChange.send(self) }}
    var handle: AuthStateDidChangeListenerHandle?
    
    func listen() {
        // monitor authentication changes using firebase
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            
            if let user = user {
                // if we have a user, create a new user model
                print("Got user: \(user)")
                self.session = User(
                    uid: user.uid,
                    displayName: user.displayName,
                    email: user.email
                )
            }else {
                // if we don't have a user, set out session to nil
                self.session = nil
            }
        })
    }
}

//MARK: additional methods (sign up, sign in) will go here
extension SessionStore {
    
    func signUp (
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback) {
        
        Auth.auth().createUser(withEmail: email, password: password, completion: handler)
    }
    
    func signIn (
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback) {
        
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
    
    func signOut () -> Bool {
        do {
            try Auth.auth().signOut()
            self.session = nil
            return true
        } catch {
            return false
        }
    }
    
    func unbind() {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
}


class AuthFireBase: ObservableObject {
    
    var ref: DatabaseReference!
    
    @Published var user: User?
    var didChange = PassthroughSubject<AuthFireBase, Never>()
    var session: User? { didSet { self.didChange.send(self) }}
    var handle: AuthStateDidChangeListenerHandle?
    
    init() {
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if let user = user {
                self.user = User(
                    uid: user.uid,
                    displayName: user.displayName, email: user.email
                )
            } else {
                self.session = nil
            }
        })
    }
    
    func signUp(
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback) {
        
        Auth.auth().createUser(withEmail: email, password: password, completion: handler)
    }
    
    func signIn(
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback) {
        
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
    
    func updateUserProfile(user: UserProfile) {
        
        self.ref = Database.database().reference().child("UserDetails")
        guard let uid: String = UserDefaultConstant.UserDetails.getValue() as? String else {
            return
        }
        self.ref.child(uid).setValue([
            // "UserImage": profileImage,
            "FirstName": user.firstName,
            "LastName": user.lastName,
            "Mobile": user.mobile,
            "Email": user.email
        ])
    }
    
    func getUserProfile(completion:@escaping (_ result: UserProfile?) -> Void) {
        let user = UserProfile()
        self.ref = Database.database().reference().child("UserDetails")
        guard let uid: String = UserDefaultConstant.UserDetails.getValue() as? String else {
            return
        }
        self.ref.child(uid).observeSingleEvent(of: .value) { (snapShot) in
            // Get user value
            let value = snapShot.value as? NSDictionary
            user.firstName = value?.value(forKey: "FirstName") as? String ?? ""
            user.lastName = value?.value(forKey: "LastName") as? String ?? ""
            user.mobile = value?.value(forKey: "Mobile") as? String ?? ""
            user.email = value?.value(forKey: "Email") as? String ?? ""
            completion(user)
        }
    }
}
