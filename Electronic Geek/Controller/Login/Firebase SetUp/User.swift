//
//  User.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 22/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

// After Firebase login - user details
class User {
    var uid: String
    var email: String?
    var displayName: String?
    
    init(uid: String, displayName: String?, email: String?) {
        self.uid = uid
        self.email = email
        self.displayName = displayName
    }
}

// User Profile
class UserProfile: Codable {
    var firstName: String?
    var lastName: String?
    var mobile: String?
    var email: String?
}
