//
//  LoginView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 20/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI
import Combine
import Firebase


class LoginActionsModel: ObservableObject {
    enum PushedItem: String {
        case registrationScreen
        case forgotPasswordScreen
        case reset
    }
}

struct LoginView: View {
    
    @State var email    = ""
    @State var password = ""
    @State var isActiveForgotPassword  = false
    @State var errorText: String = ""
    @State private var shouldAnimate = false
    @State var verifyEmail: Bool = true
    @State var crash: Int!
    
    @EnvironmentObject var session: SessionStore
    var auth = AuthFireBase()
    
    let socialButtonImages: [String] = ["share_facebook", "share_google", "share_twitter"]

    @State var selectedPushedItem: LoginActionsModel.PushedItem? = .reset
    
    var body: some View {
        NavigationView {
        
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            ScrollView(.vertical, showsIndicators: false) {
                VStack() {
                    HStack(alignment: .center) {
                        Image("logo_left")
                            .resizable()
                            .clipped()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: UIScreen.main.bounds.width/2.6, height: UIScreen.main.bounds.height/4, alignment: .leading)
                            .padding(.leading, CGFloat(10))
                            .padding(.trailing, UIScreen.main.bounds.width/5)
                        
                        Image("logo_right")
                            .resizable()
                            .clipped()
        //                            .padding(.horizontal, 1.4)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: UIScreen.main.bounds.width/2.6, height: UIScreen.main.bounds.height/4, alignment: .trailing)
    //                        .padding(.horizontal, UIScreen.main.bounds.width/10)
                            .padding(.trailing, CGFloat(10))
                    }
                    .frame(width: UIScreen.main.bounds.width)
                    .padding(.top, -50)
                    .padding(.bottom, UIScreen.main.bounds.minY - UIScreen.main.bounds.width/3)
                    
                    ImageBoyFaceWithTitle(title: "Welcome", subTitle: "Login in to Continue")
                   /*
                    VStack(alignment: .center) {
                        Text("Welcome")
                            .multilineTextAlignment(.leading)
                            .font(.custom(FontName.robotoMedium, size: 28))
                            .foregroundColor(.white)
                            .frame(width: UIScreen.main.bounds.size.width/1.2, height: 40, alignment: .leading)
                            .padding(.leading, CGFloat(0))
                            .padding(.bottom, -10)

                        Rectangle()
                            .frame(width: UIScreen.main.bounds.size.width/1.2, height: 2)
                            .padding(.horizontal, 0)
                            .foregroundColor(.yellow)
                        
                        Text("Login in to Continue")
                            .multilineTextAlignment(.leading)
                            .foregroundColor(.white)
                            .font(.custom(FontName.robotoMedium, size: 20))
                            .frame(width: UIScreen.main.bounds.size.width/1.2, height: 40, alignment: .leading)
                            .padding(.top, CGFloat(20))
                    }
                    .padding(.bottom, CGFloat(10))*/
                    
                    VStack(alignment: .leading, spacing: 20) {
                        /*
                        HStack {
                            Image("email_icon")
                                .resizable()
                                .clipped()
                                .frame(width: 25, height: 35, alignment: .leading)
                                .padding(.leading, 20)
                                .padding(.trailing, -10)
                            TextField("Email", text: self.$email)
                                .padding()
                                .cornerRadius(30.0)
                                .shadow(radius: 10.0, x: 20, y: 10)
                        }
                            .background(Color.white)
                            .clipped()
                        .overlay(RoundedRectangle(cornerRadius: 30, style: .continuous).stroke(lineWidth: 2).foregroundColor(Color.black).clipped())
                        */
                        
                        TextField("Email", text: self.$email)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(30.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                        
                        
                        SecureField("Password", text: self.$password)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(30.0)
                            .shadow(radius: 10.0, x: 20, y: 10)
                        
                        Button(action: {
                         //   let a = self.crash!
                            self.signIn()
                        }) {
                            Text("LOGIN")
                                .font(.custom(FontName.robotoBold, size: 18))
                            .foregroundColor(.white)
                            .padding()
                                .frame(width: UIScreen.main.bounds.width/1/1.1)
                            .background(Color.yellow)
                            .cornerRadius(45.0)
                            .shadow(radius: 10.0, x: 20, y: 10)
                        }
                        .padding(.top, 15)
                    }
                    .padding([.leading, .trailing], 27.5)
                    .padding(.bottom, 20)
                    .frame(width: UIScreen.main.bounds.width)
                    
                    VStack(alignment: .center) {
                        NavigationLink(destination: RegistrationView(), tag: .registrationScreen, selection: $selectedPushedItem) {
                            EmptyView()
                           Text("")
                        }
                        NavigationLink(destination: ForgotPasswordView(), tag: .forgotPasswordScreen, selection: $selectedPushedItem) {
                           Text("")
                        }
                        HStack(alignment: .lastTextBaseline, spacing: 5) {
                            Text("Don't Have Account?")
                                .multilineTextAlignment(.center)
                                .foregroundColor(.white)
                                .lineLimit(2)
                                .font(.custom(FontName.robotoRegular, size: 16))
                                .padding(.top, 20)
                                .padding(.bottom, 10)
                            Button(action: self.createAccountHereAction){
                                Text("Create Here")
                                    .multilineTextAlignment(.center)
                                    .foregroundColor(.white)
                                    .lineLimit(2)
                                    .font(.custom(FontName.robotoRegular, size: 16))
                            }
                        }
                        
                        HStack(alignment: .lastTextBaseline, spacing: 5) {
                            Text("Forgot Password?")
                                .multilineTextAlignment(.center)
                                .foregroundColor(.white)
                                .lineLimit(2)
                                .font(.custom(FontName.robotoRegular, size: 16))
                                .padding(.top, 20)
                                .padding(.bottom, 10)
                            
                            Button(action: self.resetAccountHereAction){
                                Text("Reset Here")
                                    .multilineTextAlignment(.center)
                                    .foregroundColor(.white)
                                    .lineLimit(2)
                                    .font(.custom(FontName.robotoRegular, size: 16))
                            }
//                            .sheet(isPresented: self.$isActiveForgotPassword) { () -> View in
//                                ForgotPasswordView()
//                                }
                        }
                    }
                    .padding(.bottom, 10)
                    
                    HStack(alignment: .center, spacing: 10) {
                        Rectangle()
                            .frame(width: UIScreen.main.bounds.width/3.5, height: 1, alignment: .leading)
                            .foregroundColor(.yellow)
                        Text("OR")
                            .multilineTextAlignment(.center)
                            .foregroundColor(.yellow)
                            .font(.custom(FontName.robotoRegular, size: 14))
                            .frame(width: 30, height: 30)
                        Rectangle()
                            .frame(width: UIScreen.main.bounds.width/3.5, height: 1, alignment: .trailing)
                            .foregroundColor(.yellow)
                    }
                    .padding(.bottom, 20)
                    
//                    HStack(alignment: .center) {
//                    List() {
//                        ForEach(0..<1) { _ in
                            HStack(alignment: .center, spacing: 20) {
                                ForEach(0..<3) { index in
                                    Button(action: {}) {
//                                        Text("f")
//                                            .font(.custom(FontName.robotoBold, size: 18))
//                                            .foregroundColor(self.socialButtonColor[index])
                                        Image(self.socialButtonImages[index])
                                            .resizable()
                                            .clipped()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: UIScreen.main.bounds.width/7, height: UIScreen.main.bounds.width/7)
                                            .colorMultiply(.white)
                                        
                                        .padding()
                                        .frame(width: UIScreen.main.bounds.width/7, height: UIScreen.main.bounds.width/7)
//                                        r.background(self.socialButtonColor[index])
                                        .cornerRadius(45.0)
                                        .shadow(radius: 10.0, x: 20, y: 10)
                                    }
                                    .padding(.top, 15)
                                }
                            }
//                        }
//                    }
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width/6, alignment: .center)
//                    }
                    
                    
                }
            }
    //        .background(
    //        LinearGradient(gradient: Gradient(colors: [.purple, .blue]), startPoint: .top, endPoint: .bottom)
    //            .edgesIgnoringSafeArea(.all))
        }
    }}
}

extension LoginView {
    
    func signIn() {
        auth.signIn(email: self.email, password: self.password) { (user, error) in
            if let error = error {
                self.errorText = error.localizedDescription
                    self.shouldAnimate = false

                return
                }
            guard user != nil else { return }


            self.verifyEmail = user?.user.isEmailVerified ?? false
            
            
            if(!self.verifyEmail) {
                self.errorText = "Please verify your email"
                self.shouldAnimate = false
                return
            }
         //   let userID = Auth.auth().currentUser?.uid
            UserDefaultConstant.UserDetails.setValue(user!.user.uid)
            
            self.email = ""
            self.verifyEmail = true
            self.password = ""
            self.errorText = ""
            self.shouldAnimate = false
            NotificationCenter.default.post(name: homeRootViewNotificaton, object: nil)
        }
    }
    
    fileprivate func createAccountHereAction() {
        self.selectedPushedItem = .registrationScreen
    }
        
    fileprivate func resetAccountHereAction() {
        self.selectedPushedItem = .forgotPasswordScreen
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
            .environmentObject(SessionStore())
    }
}
