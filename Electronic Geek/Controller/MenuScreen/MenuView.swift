//
//  MenuView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 06/08/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI
import Combine

class MenuViewAction: ObservableObject {
    enum pushItemSelection: String {
        case profile
        case message
        case reset
    }
}

struct MenuView: View {
    
    @State var showMenu = false
    @State var selectedPushItem: MenuViewAction.pushItemSelection = .reset
    
    var body: some View {
        ZStack() {
            Color.black.edgesIgnoringSafeArea(.all)
            VStack(alignment: .leading) {
                
//                NavigationLink(destination: ProfileView(), tag: .profile, selection: $selectedPushItem) {
//                   //  EmptyView()
////                    Text("")
//                }
                HStack() {
                    Button(action: {
                        self.showProfileScreen()
                    }){
                        Image(systemName: "person")
                            .foregroundColor(.gray)
                            .imageScale(.large)
                        Text("Profile")
                            .foregroundColor(.gray)
                            .font(.headline)
                    }
                }
                .padding(.top, 100)
                HStack() {
                    Button(action: {
                        
                    }){
                        Image(systemName: "envelope")
                            .foregroundColor(.gray)
                            .imageScale(.large)
                        Text("Messages")
                            .foregroundColor(.gray)
                            .font(.headline)
                    }
                }
                .padding(.top, 30)
                HStack() {
                    Image(systemName: "gear")
                        .foregroundColor(.gray)
                        .imageScale(.large)
                    Text("Settings")
                        .foregroundColor(.gray)
                        .font(.headline)
                }
                .padding(.top, 30)
                Spacer()
            }
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            .background(Color(red: 32/255, green: 32/255, blue: 32/255))
            .edgesIgnoringSafeArea(.all)
        }
    }
}

extension MenuView {
    
    fileprivate func showProfileScreen() {
        self.selectedPushItem = .profile
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
