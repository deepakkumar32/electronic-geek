//
//  ProfileView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 21/08/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI
import Combine
import Firebase

struct ProfileView: View {
    
    @State var firstName = ""
    @State var lastName = ""
    @State var mobile = ""
    @State var email = ""
    @State var isActiveForgotPassword  = false
    @State var errorText: String = ""
    @State private var shouldAnimate = false
    
    @State var showImagePicker: Bool = false
    @State var image: Image? = nil
    @State var img = UIImage(named: "boy_face")
    
    var firebase = AuthFireBase()
    var userDetails = UserProfile()
    @State var showLoader: Bool = true
    
    var body: some View {
        LoadingView(isShowing: .constant(self.showLoader)) {
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            ScrollView(.vertical, showsIndicators: false) {
                VStack() {
                    Button(action: {
                        self.showImagePicker.toggle()
                    }) {
                        Image("boy_face")
                            .resizable()
                            .clipped()
                            .clipShape(Circle())
                            // .aspectRatio(contentMode: .fit)
                            .frame(width: UIScreen.main.bounds.width/2.5, height: UIScreen.main.bounds.width/2.5)
                            .padding(.top, 20)
                            .padding(.bottom, 40)
                        //                            .shadow(radius: 10.0, x: 20, y: 10)
                    }
                    
                    self.doubleHorizontalTextFields(txt1Placeholder: "First Name", txt2Placeholder: "Last Name")
                        .padding(.bottom, CGFloat(-10))
                    
                    TextField("Mobile No.", text: self.$mobile)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(30.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                        .padding([.leading, .trailing], 10)
                        .padding(.bottom, CGFloat(10))
                    
                    TextField("Email ID", text: self.$email)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(30.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                        .padding([.leading, .trailing], 10)
                        .padding(.bottom, CGFloat(10))
                    
                    
                    
                    //                    doubleHorizontalTextFields(txt1Placeholder: "First Name", txt2Placeholder: "Last Name")
                    //                        .padding(.bottom, CGFloat(10))
                    
                    Button(action: {
                        self.updateUserProfile()
                    }) {
                        Text("Update")
                            .font(.custom(FontName.robotoBold, size: 18))
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: UIScreen.main.bounds.width/1/1.1)
                            .background(Color.yellow)
                            .cornerRadius(45.0)
                            .shadow(radius: 10.0, x: 20, y: 10)
                    }
                    .padding(.top, 15)
                }
                .sheet(isPresented: self.$showImagePicker) {
                    ImagePicker(sourceType: .photoLibrary) { image in
                        self.image = Image(uiImage: image)
                    }
                }
            }
        }.onAppear() {
            self.getUserProfileDetails()
        }
        }}
}

extension ProfileView {
    
    fileprivate func doubleHorizontalTextFields(txt1Placeholder: String, txt2Placeholder: String) -> some View {
        return HStack(alignment: .center, spacing: 20) {
            
            TextField(txt1Placeholder, text: self.$firstName)
                .padding()
                .background(Color.white)
                .cornerRadius(30.0)
                .shadow(radius: 10.0, x: 20, y: 10)
            // .frame(height: 30)
            
            
            TextField(txt2Placeholder, text: self.$lastName)
                .padding()
                .background(Color.white)
                .cornerRadius(30.0)
                .shadow(radius: 10.0, x: 20, y: 10)
            
        }
        .padding([.leading, .trailing], 10)
        .padding(.bottom, 20)
        .frame(width: UIScreen.main.bounds.width)
    }
    
    fileprivate func updateUserProfile() {
        userDetails.firstName = self.firstName
        userDetails.lastName = self.lastName
        userDetails.email = self.email
        userDetails.mobile = self.mobile
        firebase.updateUserProfile(user: userDetails)
    }
    
    fileprivate func getUserProfileDetails() {
        self.firebase.getUserProfile { (result: UserProfile?) in
            self.firstName = result?.firstName ?? ""
            self.lastName = result?.lastName ?? ""
            self.mobile = result?.mobile ?? ""
            self.email = result?.email ?? ""
            self.showLoader = false
        }
    }
}
struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}


public struct ImagePicker: UIViewControllerRepresentable {
    
    private let sourceType: UIImagePickerController.SourceType
    private let onImagePicked: (UIImage) -> Void
    @Environment(\.presentationMode) private var presentationMode
    
    public init(sourceType: UIImagePickerController.SourceType, onImagePicked: @escaping (UIImage) -> Void) {
        self.sourceType = sourceType
        self.onImagePicked = onImagePicked
    }
    
    public func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.sourceType = self.sourceType
        picker.delegate = context.coordinator
        return picker
    }
    
    public func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}
    
    public func makeCoordinator() -> Coordinator {
        Coordinator(
            onDismiss: { self.presentationMode.wrappedValue.dismiss() },
            onImagePicked: self.onImagePicked
        )
    }
    
    final public class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        private let onDismiss: () -> Void
        private let onImagePicked: (UIImage) -> Void
        
        init(onDismiss: @escaping () -> Void, onImagePicked: @escaping (UIImage) -> Void) {
            self.onDismiss = onDismiss
            self.onImagePicked = onImagePicked
        }
        
        public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
            if let image = info[.originalImage] as? UIImage {
                self.onImagePicked(image)
            }
            self.onDismiss()
        }
        
        public func imagePickerControllerDidCancel(_: UIImagePickerController) {
            self.onDismiss()
        }
    }
}


