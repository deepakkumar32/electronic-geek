//
//  RegistrationView.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 23/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI
import Firebase

struct RegistrationView: View {
    
    @State var email    = ""
    @State var password = ""
    @State var nickName = ""
    @State var errorText: String = ""
    @State private var showAlert = false

    @State var selectedPushedItem: LoginActionsModel.PushedItem?
    
    let avatarCollection: [Image] = [Image("avatar1"), Image("avatar2"), Image("avatar3"), Image("avatar4"), Image("avatar5"), Image("avatar6"), Image("avatar7"), Image("avatar8")]

    var body: some View {
        ZStack {
            Color.defaultAppThemeColor.edgesIgnoringSafeArea(.all)
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack() {
                    ImageBoyFaceWithTitle(title: "Register", subTitle: "Create a free account")
                    
                    VStack(alignment: .leading, spacing: 20) {
                       
                        TextField("Nick Name", text: self.$nickName)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(30.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                        
                        
                        TextField("Email", text: self.$email)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(30.0)
                        .shadow(radius: 10.0, x: 20, y: 10)
                        
                        
                        SecureField("Password", text: self.$password)
                            .padding()
                            .background(Color.white)
                            .cornerRadius(30.0)
                            .shadow(radius: 10.0, x: 20, y: 10)
                            .padding(.bottom, 10)
                        
                        Text("Choose avatar")
                            .font(.custom(FontName.robotoRegular, size: 18))
                            .foregroundColor(Color.white)
                        /*
                        HStack {
                            Image("email_icon")
                                .resizable()
                                .clipped()
                                .frame(width: 25, height: 35, alignment: .leading)
                                .padding(.leading, 20)
                                .padding(.trailing, -10)
                        }
                            .background(Color.white)
                            .frame(width: UIScreen.main.bounds.width - 40, height: 50, alignment: .center)
                            .clipped()
                        .overlay(RoundedRectangle(cornerRadius: 5, style: .continuous).stroke(lineWidth: 2).foregroundColor(Color.black).clipped())
                        */
                        
                        /*HStack() {
                            ForEach(0..<3) { index in
                                Button(action: {}) {
                                    Image(self.socialButtonImages[index])
                                        .resizable()
                                        .clipped()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: UIScreen.main.bounds.width/7, height: UIScreen.main.bounds.width/7)
                                        .colorMultiply(.white)
                                    
                                    .padding()
//                                    .frame(width: UIScreen.main.bounds.width/7, height: UIScreen.main.bounds.width/7)
                                    .cornerRadius(45.0)
                                    .shadow(radius: 10.0, x: 20, y: 10)
                                }
                                .padding(.top, 15)
                            }
                        }*/
                        
                        HStack(alignment: .center, spacing: 5) {
                            ForEach(0..<self.avatarCollection.count) { index in
                                VStack{
                                    
                                    Button(action: {}){
                                        Text("")
                                    }
                                    .mask(Circle())
                                    .frame(width: 20, height: 20, alignment: .center)
                                    .background(Color.clear)
                                    .border(Color.red, width: 1)
                                    .clipped()
                            
                                    
                                    self.avatarCollection[index]
                                    .resizable()
                                    .clipped()
                                    .aspectRatio(contentMode: .fit)
                                }
                                .frame(width: 60, height: UIScreen.main.bounds.width/7)
                            }
                        }
                        .background(Color.clear)
                        .frame(width: UIScreen.main.bounds.width-40, height: UIScreen.main.bounds.width/7)
                        
                        
                        registrationBtn()
                    }
                    .padding([.leading, .trailing], 90)
                    .padding(.bottom, 20)
                    .frame(width: UIScreen.main.bounds.width)
                }
            }
        }
//        .navigationBarBackButtonHidden(true)
//        .navigationBarItems(leading: BackButton(label: "Back!") {
//            self.selectedPushedItem = .reset
//        })
    }
}

extension RegistrationView {
    
    fileprivate func registrationBtn() -> some View {
        return Button(action: {
            self.signUp()
        }) {
            Text("CREATE ACCOUNT")
                .font(.custom(FontName.robotoBold, size: 18))
                .foregroundColor(.white)
                .padding()
                .frame(width: UIScreen.main.bounds.width/1/1.1)
                .background(Color.yellow)
                .cornerRadius(45.0)
                .shadow(radius: 10.0, x: 20, y: 10)
        }
        .padding(.top, 15)
    }
    
    func signUp () {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            guard let user = authResult?.user, error == nil else {
                print(error?.localizedDescription ?? "unknown error !!")
                return
            }
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                if let error = error {
                    self.errorText = error.localizedDescription
                    return
                }
                self.showAlert.toggle()
            })
            print("\(user.email!) created")
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}


/*
self.selectedPushedItem = .reset
.navigationBarBackButtonHidden(true)
.navigationBarItems(leading: BackButton(label: "Back!") {
    self.selectedPushedItem = .reset
})

*/
