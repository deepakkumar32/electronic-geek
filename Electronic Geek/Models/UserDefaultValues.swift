//
//  UserDefaultValues.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 26/08/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import Firebase

enum UserDefaultConstant: String {
    
    case UserDetails
    
    func setValue(_ value: Any) {
        switch self {
            
            case .UserDetails: if value is String {}; break
        }
        
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func getValue() -> Any? {
        switch self {
            case .UserDetails: break
        }
        
        guard let data = UserDefaults.standard.object(forKey: self.rawValue) as? String else {
            return ""
        }; return data
    }
}
