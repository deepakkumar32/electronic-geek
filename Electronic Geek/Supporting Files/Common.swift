//
//  Common.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 16/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import SwiftUI


let loginRootViewNotification = NSNotification.Name("loginRootViewNotification")
let homeRootViewNotificaton = NSNotification.Name("homeRootViewNotification")


// MARK: Font Family Names
struct FontName {
    static let plaster              : String = "Plaster-Regular"
    static let robotoBold           : String = "Roboto-Bold"
    static let robotoBoldItalic     : String = "Roboto-BoldItalic"
    static let robotoBlack          : String = "Roboto-Black"
    static let robotoBlackItalic    : String = "Roboto-BlackItalic"
    static let robotoItalic         : String = "Roboto-Italic"
    static let robotoLight          : String = "Roboto-Light"
    static let robotoLightItalic    : String = "Roboto-LightItalic"
    static let robotoMedium         : String = "Roboto-Medium"
    static let robotoMediumItalic   : String = "Roboto-MediumItalic"
    static let robotoRegular        : String = "Roboto-Regular"
    static let robotoThin           : String = "Roboto-Thin"
    static let robotoThinItalic     : String = "Roboto-ThinItalic"
}


// MARK: Activity Indicator
struct ActivityIndicator: UIViewRepresentable {
    
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

struct LoadingView<Content>: View where Content: View {
    
    @Binding var isShowing: Bool
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                
                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 1 : 0)
                
                VStack {
                    Text("Loading...")
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                   /*
                    ForEach(0..<5) { index in
                        Group {
                          Circle()
                            .frame(width: geometry.size.width / 5, height: geometry.size.height / 5)
                            .scaleEffect(!self.isAnimating ? 1 - CGFloat(index) / 5 : 0.2 + CGFloat(index) / 5)
                            .offset(y: geometry.size.width / 10 - geometry.size.height / 2)
                          }.frame(width: geometry.size.width, height: geometry.size.height)
                            .rotationEffect(!self.isShowing ? .degrees(0) : .degrees(360))
                            .animation(Animation
                              .timingCurve(0.5, 0.15 + Double(index) / 5, 0.25, 1, duration: 1.5)
                              .repeatForever(autoreverses: false))
                        }*/
                }
                .frame(width: geometry.size.width / 2,
                       height: geometry.size.height / 5)
                    .background(Color.secondary.colorInvert())
                    .foregroundColor(Color.primary)
                    .cornerRadius(20)
                    .opacity(self.isShowing ? 1 : 0)
                
            }
        }
    }
    
}

