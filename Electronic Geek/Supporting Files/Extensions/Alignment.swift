//
//  Alignment.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 22/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

extension HorizontalAlignment {
    struct CenterAlignment: AlignmentID {
        static func defaultValue(in frame: ViewDimensions) -> CGFloat {
            frame[.top]
        }
    }
    static let centerAlignment = HorizontalAlignment(CenterAlignment.self)
}
