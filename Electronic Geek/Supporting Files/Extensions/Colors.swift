//
//  Colors.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 22/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

extension Color {
    static let defaultAppThemeColor = Color(red: 203/255.0, green: 147/255.0, blue: 47/255.0)
}
