//
//  BackButton.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 31/07/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct BackButton: View {
    let label: String
    let closure: () -> ()
    
    var body: some View {
        Button(action: { self.closure() }) {
            HStack {
                Image(systemName: "chevron.left")
                Text(label)
            }
        }
    }
}

//struct BackButton_Previews: PreviewProvider {
//    static var previews: some View {
//        BackButton()
//    }
//}


