//
//  CountSubViewGeekSubjects.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 06/08/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct CountSubViewGeekSubjects: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CountSubViewGeekSubjects_Previews: PreviewProvider {
    static var previews: some View {
        CountSubViewGeekSubjects()
    }
}
