//
//  ImageBoyFace.swift
//  Electronic Geek
//
//  Created by Deepak Jaswal on 23/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct ImageBoyFaceWithTitle: View {
    
    var title = String()
    var subTitle = String()
    
    var body: some View {
        VStack() {
            Image("boy_face")
            .resizable()
            .clipped()
            .aspectRatio(contentMode: .fit)
            .frame(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.height/2.6)
            .padding(.horizontal, -UIScreen.main.bounds.width/6)
            .padding(.bottom, -40)
            
            VStack(alignment: .center) {
                Text(title)
                    .multilineTextAlignment(.leading)
                    .font(.custom(FontName.robotoMedium, size: 28))
                    .foregroundColor(.white)
                    .frame(width: UIScreen.main.bounds.size.width/1.2, height: 40, alignment: .leading)
                    .padding(.leading, CGFloat(0))
                    .padding(.bottom, -10)

                Rectangle()
                    .frame(width: UIScreen.main.bounds.size.width/1.2, height: 2)
                    .padding(.horizontal, 0)
                    .foregroundColor(.yellow)
                
                Text(subTitle)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.white)
                    .font(.custom(FontName.robotoMedium, size: 20))
                    .frame(width: UIScreen.main.bounds.size.width/1.2, height: 40, alignment: .leading)
                    .padding(.top, CGFloat(10))
            }
            .padding(.bottom, CGFloat(10))
        }
    }
}

struct ImageBoyFace_Previews: PreviewProvider {
    static var previews: some View {
        ImageBoyFaceWithTitle()
    }
}
