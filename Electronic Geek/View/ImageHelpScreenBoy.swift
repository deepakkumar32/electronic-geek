//
//  ImageHelpScreenBoy.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 17/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct ImageHelpScreenBoy: View {
    var body: some View {
        Image("boy")
        .resizable()
        .clipped()
        .aspectRatio(contentMode: .fit)
        .frame(width: UIScreen.main.bounds.width/2.5, height: UIScreen.main.bounds.height/4)
        .padding(.horizontal, -UIScreen.main.bounds.width/10)
        .padding(.trailing, -10)
    }
}
struct ImageHelpScreenBoy_Previews: PreviewProvider {
    static var previews: some View {
        ImageHelpScreenBoy()
    }
}
