//
//  ImageHelpScreenChip.swift
//  Electronic Geek
//
//  Created by Deepak Kumar on 17/04/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import SwiftUI

struct ImageHelpScreenChip: View {
    var body: some View {
        Image("chip")
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(width: UIScreen.main.bounds.width/10, height: UIScreen.main.bounds.height/16, alignment: .bottomTrailing)
        .padding(.bottom, 0)
    }
}

struct ImageHelpScreenChip_Previews: PreviewProvider {
    static var previews: some View {
        ImageHelpScreenChip()
    }
}
